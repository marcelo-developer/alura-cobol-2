       IDENTIFICATION DIVISION.
       PROGRAM-ID. CLIENTES.
      **************************
      * OBJETIVO: SISTEMA DE GESTAO DE CLIENTES
      * AUTHOR  : MARCELO
      **************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT CLIENTES ASSIGN TO 'CLIENTES.DAT'
               ORGANIZATION IS INDEXED
               ACCESS MODE IS RANDOM
               FILE STATUS IS CLIENTES-STATUS
               RECORD KEY IS CLIENTES-CHAVE.
           SELECT RELATO ASSIGN TO 'RELATORIO.TXT'
               ORGANIZATION IS SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD CLIENTES.
           01 CLIENTES-REG.
               05 CLIENTES-CHAVE.
                   10 CLIENTES-FONE PIC 9(09).
               05 CLIENTES-NOME     PIC X(30).
               05 CLIENTES-EMAIL    PIC X(40).
       FD RELATO.
           01 RELATO-REG.
               05 RELATO-DADOS PIC X(79).
               05 RELATO-BREAK PIC X(01).
       WORKING-STORAGE SECTION.
           77 WRK-OPCAO         PIC X(01).
           77 WRK-OPCAO-RELATO  PIC X(01).
           77 WRK-MODULO        PIC X(25).
           77 WRK-TECLA         PIC X(01).
           77 WRK-MSGERRO       PIC X(40).
           77 WRK-CONTALINHA    PIC 9(03).
           77 WRK-QTREGISTROS   PIC 9(05) VALUE 0.
           77 CLIENTES-STATUS   PIC 9(02) VALUE 0.
       SCREEN SECTION.
           01 TELA.
               05 LIMPA-TELA.
                   10 BLANK SCREEN.
                   10 LINE 01 COLUMN 01 ERASE EOL
                       BACKGROUND-COLOR 3.
                   10 LINE 01 COLUMN 32 VALUE 'SISTEMA DE CLIENTES'
                       BACKGROUND-COLOR 3 FOREGROUND-COLOR 0.
                   10 LINE 02 COLUMN 01 PIC X(25) ERASE EOL
                       FROM WRK-MODULO
                       BACKGROUND-COLOR 1.
           01 MENU.
               05 LINE 07 COLUMN 15 VALUE '1 - INCLUIR'.
               05 LINE 08 COLUMN 15 VALUE '2 - CONSULTAR'.
               05 LINE 09 COLUMN 15 VALUE '3 - ALTERAR'.
               05 LINE 10 COLUMN 15 VALUE '4 - EXCLUIR'.
               05 LINE 11 COLUMN 15 VALUE '5 - RELATORIO'.
               05 LINE 12 COLUMN 15 VALUE 'X - SAIDA'.
               05 LINE 13 COLUMN 15 VALUE 'OPCAO.......: '.
               05 LINE 13 COLUMN 28 USING WRK-OPCAO.
           01 MENU-RELATO.
               05 LINE 11 COLUMN 45 VALUE '1 - EM TELA'.
               05 LINE 12 COLUMN 45 VALUE '2 - EM DISCO'.
               05 LINE 13 COLUMN 45 VALUE 'OPCAO.......: '.
               05 LINE 13 COLUMN 58 USING WRK-OPCAO-RELATO.
           01 TELA-REGISTRO.
               05 CHAVE FOREGROUND-COLOR 2.
                   10 LINE 10 COLUMN 10 VALUE 'TELEFONE '.
                   10 COLUMN PLUS 2 PIC 9(09) USING CLIENTES-FONE
                       BLANK WHEN ZEROS.
               05 SS-DADOS.
                   10 LINE 11 COLUMN 10 VALUE 'NOME.... '.
                   10 COLUMN PLUS 2 PIC X(30) USING CLIENTES-NOME.
                   10 LINE 12 COLUMN 10 VALUE 'EMAIL... '.
                   10 COLUMN PLUS 2 PIC X(40) USING CLIENTES-EMAIL.
           01 MOSTRA-ERRO.
               02 MSG-ERRO.
                   03 LINE 16 ERASE EOL BACKGROUND-COLOR 3.
                   03 LINE 16 COLUMN 10 PIC X(40)
                       BACKGROUND-COLOR 3
                       FROM WRK-MSGERRO.
                   03 COLUMN PLUS 2 PIC X(01)
                       BACKGROUND-COLOR 3
                       USING WRK-TECLA.
       PROCEDURE DIVISION.
       0001-PRINCIPAL.
           PERFORM 1000-INICIAR THRU 1100-MONTATELA.
           PERFORM 2000-PROCESSAR UNTIL
                WRK-OPCAO = 'X' OR WRK-OPCAO = 'x'.
           PERFORM 3000-FINALIZAR.
           STOP RUN.
       1000-INICIAR.
           OPEN I-O CLIENTES.
           IF CLIENTES-STATUS = 35 THEN
               OPEN OUTPUT CLIENTES
               CLOSE CLIENTES
               OPEN I-O CLIENTES
           END-IF.
       1100-MONTATELA.
           MOVE 'MENU' TO WRK-MODULO.
           DISPLAY TELA.
           ACCEPT MENU.
       2000-PROCESSAR.
           MOVE SPACES TO CLIENTES-NOME CLIENTES-EMAIL WRK-MSGERRO
                          WRK-TECLA.
           MOVE ZERO TO WRK-QTREGISTROS.
           EVALUATE WRK-OPCAO
               WHEN 1
                   PERFORM 5000-INCLUIR
               WHEN 2
                   PERFORM 6000-CONSULTAR
               WHEN 3
                   PERFORM 7000-ALTERAR
               WHEN 4
                   PERFORM 8000-EXCLUIR
               WHEN 5
                   ACCEPT MENU-RELATO
                   IF WRK-OPCAO-RELATO EQUAL 1
                       PERFORM 9000-RELATORIO-TELA
                   END-IF
                   IF WRK-OPCAO-RELATO EQUAL 2
                       PERFORM 9100-RELATORIO-DISCO
                   END-IF
               WHEN OTHER
                   IF WRK-OPCAO NOT EQUAL 'X'
                       DISPLAY 'ENTRE COM A OPCAO CORRETA'
                   END-IF
           END-EVALUATE.
           PERFORM 1100-MONTATELA.
       3000-FINALIZAR.
           CLOSE CLIENTES.
       5000-INCLUIR.
           MOVE 'MODULO - INCLUSAO' TO WRK-MODULO.
           DISPLAY TELA.
           ACCEPT TELA-REGISTRO.
           WRITE CLIENTES-REG
               INVALID KEY
                   MOVE 'JA EXISTE! (N)OVO REGISTRO?' TO WRK-MSGERRO
                   ACCEPT MOSTRA-ERRO
                   IF WRK-TECLA = 'N' OR WRK-TECLA = 'n'
                       MOVE ZEROS TO CLIENTES-FONE
                       PERFORM 5000-INCLUIR
                   END-IF
           END-WRITE.
       6000-CONSULTAR.
           MOVE 'MODULO - CONSULTA' TO WRK-MODULO.
           DISPLAY TELA.
           DISPLAY TELA-REGISTRO.
           ACCEPT CHAVE.
           READ CLIENTES
               INVALID KEY
                   MOVE 'NAO ENCONTRADO ' TO WRK-MSGERRO
               NOT INVALID KEY
                   MOVE 'ENCONTRADO ' TO WRK-MSGERRO
                   DISPLAY SS-DADOS
           END-READ.
           ACCEPT MOSTRA-ERRO.
       7000-ALTERAR.
           MOVE 'MODULO - ALTERACAO' TO WRK-MODULO.
           DISPLAY TELA.
           DISPLAY TELA-REGISTRO.
           ACCEPT CHAVE.
           READ CLIENTES.
           IF CLIENTES-STATUS = 0
               ACCEPT SS-DADOS
               REWRITE CLIENTES-REG
               IF CLIENTES-STATUS = 0
                   MOVE 'ALTERADO' TO WRK-MSGERRO
               ELSE
                   MOVE 'NAO ALTERADO' TO WRK-MSGERRO
               END-IF
           ELSE
               MOVE 'REGISTRO NAO ENCONTRADO' TO WRK-MSGERRO
           END-IF.
           ACCEPT MOSTRA-ERRO.
       8000-EXCLUIR.
           MOVE 'MODULO - EXCLUSAO' TO WRK-MODULO.
           DISPLAY TELA.
           DISPLAY TELA-REGISTRO.
           ACCEPT CHAVE.
           READ CLIENTES
               INVALID KEY
                   MOVE 'NAO ENCONTRADO ' TO WRK-MSGERRO
               NOT INVALID KEY
                   MOVE 'ENCONTRADO. EXCLUIR? (S/N) ' TO WRK-MSGERRO
                   DISPLAY SS-DADOS
           END-READ.
           ACCEPT MOSTRA-ERRO.
           IF (WRK-TECLA = 'S' OR WRK-TECLA = 's')
                   AND CLIENTES-STATUS = 0
               DELETE CLIENTES
                   INVALID KEY
                       MOVE 'NAO EXCLUIDO ' TO WRK-MSGERRO
                   NOT INVALID KEY
                       MOVE 'EXCLUIDO ' TO WRK-MSGERRO
               END-DELETE
               ACCEPT MOSTRA-ERRO
           END-IF.
       9000-RELATORIO-TELA.
           MOVE 'MODULO - RELATORIO' TO WRK-MODULO.
           DISPLAY TELA.
           DISPLAY 'REGISTRO INICIAL' AT 0810.
           ACCEPT CHAVE.
           READ CLIENTES
               INVALID KEY
                   MOVE 'NENHUM REGISTRO ENCONTRADO' TO WRK-MSGERRO
               NOT INVALID KEY
                   DISPLAY '    RELATORIO DE CLIENTES    '
                   DISPLAY '-----------------------------'
                   PERFORM UNTIL CLIENTES-STATUS = 10
                       ADD 1 TO WRK-QTREGISTROS
                       DISPLAY CLIENTES-FONE ' '
                               CLIENTES-NOME ' '
                               CLIENTES-EMAIL
                       READ CLIENTES NEXT
                       ADD 1 TO WRK-CONTALINHA
                       IF WRK-CONTALINHA = 5
                           MOVE 'PRESSIONE ALGUMA TECLA ' TO WRK-MSGERRO
                           ACCEPT MOSTRA-ERRO
                           MOVE 'MODULO - RELATORIO' TO WRK-MODULO
                           DISPLAY TELA
                           DISPLAY '    RELATORIO DE CLIENTES    '
                           DISPLAY '-----------------------------'
                           MOVE 0 TO WRK-CONTALINHA
                       END-IF
                   END-PERFORM
           END-READ.
           MOVE 'REGISTROS LIDOS ' TO WRK-MSGERRO.
           MOVE WRK-QTREGISTROS TO WRK-MSGERRO(17:5).
           ACCEPT MOSTRA-ERRO.
       9100-RELATORIO-DISCO.
           MOVE 'MODULO - RELATORIO' TO WRK-MODULO.
           DISPLAY TELA.
           DISPLAY 'REGISTRO INICIAL' AT 0810.
           ACCEPT CHAVE.
           READ CLIENTES
               INVALID KEY
                   MOVE 'NENHUM REGISTRO ENCONTRADO' TO WRK-MSGERRO
               NOT INVALID KEY
                   OPEN OUTPUT RELATO
                   MOVE X'0a' TO RELATO-BREAK
                   PERFORM UNTIL CLIENTES-STATUS = 10
                       ADD 1 TO WRK-QTREGISTROS
                       MOVE CLIENTES-REG TO RELATO-DADOS
                       WRITE RELATO-REG
                       READ CLIENTES NEXT
                   END-PERFORM
                   MOVE 'REGISTROS LIDOS ' TO RELATO-DADOS
                   MOVE WRK-QTREGISTROS TO RELATO-DADOS(17:5)
                   WRITE RELATO-REG
                   CLOSE RELATO
           END-READ.
           MOVE 'LINHAS GRAVADAS ' TO WRK-MSGERRO
           MOVE WRK-QTREGISTROS TO WRK-MSGERRO(17:5)
           ACCEPT MOSTRA-ERRO.
